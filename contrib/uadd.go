package main

import (
	"api"
	"fmt"
	"os"
	"common"
	"golang.org/x/crypto/bcrypt"
	"gopkg.in/mgo.v2"
)

func main() {
	hash, err := bcrypt.GenerateFromPassword([]byte(os.Args[2]), 0)
	if err != nil {
		fmt.Printf("Cannot hash password: %s\n", err.Error())
		return
	}

	dbc := xh.ParseXCreds(os.Args[3])
	info := mgo.DialInfo{
		Addrs:		[]string{dbc.Addr()},
		Database:	api.DBStateDB,
		Username:	dbc.User,
		Password:	dbc.Pass,
	}

	s, err := mgo.DialWithInfo(&info);
	if err != nil {
		fmt.Printf("Cannot connect to db: %s\n", err.Error())
		return
	}

	err = s.DB(api.DBStateDB).C(api.DBColUsers).Insert(
		&api.UserEntry { Name: os.Args[1], PassHash: hash },
	)
	if err != nil {
		fmt.Printf("Cannot insert user: %s\n", err.Error())
		return
	}
}

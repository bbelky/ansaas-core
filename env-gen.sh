#!/usr/bin/env bash
cat <<EOF > .env
MONGO_INITDB_ROOT_USERNAME=root
MONGO_INITDB_ROOT_PASSWORD=$(openssl rand -base64 12)
MONGODB_DATABASE=ansaas
DBUSER=ansaas
DBPASS=$(openssl rand -base64 12)
MQUSER=ansaas
MQPASS=$(openssl rand -base64 12)
RABBITMQ_DEFAULT_VHOST=ansaas
JWTKEY0=$(openssl rand -hex 12)
VOLUME="/home/ansaas"
EOF
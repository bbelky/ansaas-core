### Build

<pre>
export GOPATH=$(pwd)/vendor:$(pwd)
./deps.sh
</pre>
to have all deps installed in cwd, or just
<pre>
export GOPATH=$GOPATH:$(pwd)
</pre>
then
<pre>
make
</pre>


### Manual starting

**Prepare aux services**

1. Add "ansaas" db to mongo and one (or two) users to it
2. Add "ansaas" vhost to rabbit amd one (or two) users to it
3. Prepare the store (it must be shared between node with daemon and node with worker(s))
<pre>
mkdir /home/ansaas && cd /home/ansaas
mkdir keys
mkdir artifacts
mkdir playbooks && cd playbooks
mkdir kubespray && cd kubespray
git clone https://github.com/kubernetes-sigs/kubespray repo
ln -s /root/ansaas/pb-runs/kubespray.sh run.sh
</pre>
4. Prepare the environment file:
<pre>
chmod +x env-gen.sh && ./env-gen.sh
source .env
</pre>
5. Add user to play with
```
source .env
pass=$(openssl rand -base64 12) #or you can assign to variable your regular password
go run contrib/uadd.go "me" "$pass" "$DBUSER:$DBUSER@mongo:port"
```

**Run the daemon and worker(s)**

<pre>
make daemon-img
make worker-img
docker/$service/run.sh $path-to-env-file $any-other-option-to-docke-run-cmd
</pre>

### Run services with docker-compose

**Install additional software**

1. Install [docker](https://docs.docker.com/install/)
2. Install [docker-compose](https://docs.docker.com/compose/install/)

**Generate .env file**

*This file contains all usernames and passwords for ansaas*

```
chmod +x env-gen.sh && ./env-gen.sh
```

**Create services**

```
docker-compose up -d --scale ansaas-worker=5
```

**Stop/start services**

```
docker-compose stop
docker-compose start
```

**Scale workers**

```
docker-compose stop
docker-compose up -d --scale ansaas-worker=10
```

**Remove services**

*This command doesn't remove ansaas docker volumes and ansaas docker images*

```
docker-compose down
```

**Clean up all**

*This command completely remove all*
```
docker-compose down -v --rmi all
```

### Play with the stuff

<pre>
  D="http://127.0.0.1:8681/v1"

  curl -v -X POST "$D/login"    -H 'Content-Type: application/json' -d '{"username":"me","password":...}'
  T="X-Auth-Token: ..." (X-Subject-Token response header)

  curl -v -X POST -H "$T" "$D/clusters" -H 'Content-Type: application/json' -d '{"name":"cluster1"}'

  ID=... # ID of the culter created

  curl -v -X POST -H "$T" "$D/clusters/${ID}/sshkey/make"
  curl -v -X GET  -H "$T" "$D/clusters/${ID}/sshkey"

  Minimal setup - 3 nodes
  
  curl -v -X POST -H "$T" "$D/clusters/${ID}/nodes" -H 'Content-Type: application/json' -d '{"name":"node1","addr":"1.2.3.4"}'
  curl -v -X POST -H "$T" "$D/clusters/${ID}/nodes" -H 'Content-Type: application/json' -d '{"name":"node2","addr":"1.2.3.5"}'
  curl -v -X POST -H "$T" "$D/clusters/${ID}/nodes" -H 'Content-Type: application/json' -d '{"name":"node3","addr":"1.2.3.6"}'
  
  curl -v -X POST -H "$T" "$D/clusters/${ID}/deploys" -H 'Content-Type: application/json' -d '{"playbook": "kubespray"}'
</pre>

After this step the pb-runs/kubespray.sh script will be launced by worker.

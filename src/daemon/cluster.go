package main

import (
	"log"
	"gopkg.in/mgo.v2/bson"
	"gopkg.in/mgo.v2"
	"api"
	"xrest"
	"context"
	"net/http"
	"net/url"
)

type ClusterDesc struct {
	ObjID		bson.ObjectId		`bson:"_id,omitempty"`
	User		string			`bson:"user"`
	Name		string			`bson:"name"`
	Nodes		map[string]*NodeDesc	`bson:"nodes"`
	Keys		string			`bson:"keys"`
}

func (cl *ClusterDesc)Add(ctx context.Context, p interface{}) *xrest.ReqErr {
	cl.ObjID = bson.NewObjectId()

	err := dbInsert(ctx, cl)
	if err != nil {
		return DErrD(err)
	}

	return nil
}

func (cl *ClusterDesc)Upd(ctx context.Context, upd interface{}) *xrest.ReqErr {
	return DErrM(xrest.GenErr, "Not updatable")
}

func (cl *ClusterDesc)Del(ctx context.Context) *xrest.ReqErr {
	if cl.Keys != "" {
		sshKeydrop(ctx, cl.Keys)
	}

	err := dbRemove(ctx, cl)
	if err != nil {
		return DErrD(err)
	}

	return nil
}

func (cl *ClusterDesc)lastDep(ctx context.Context) (*DeployDesc, error) {
	var ld DeployDesc

	err := dbFindDep(ctx, bson.M{"cluster_id": cl.ObjID}).Sort("-started").Limit(1).One(&ld)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = nil
		}

		return nil, err
	}

	return &ld, nil
}

func (cl *ClusterDesc)Info(ctx context.Context, q url.Values, details bool) (interface{}, *xrest.ReqErr) {
	ci := &api.ClusterInfo {
		Id:	cl.ObjID.Hex(),
		Name:	cl.Name,
	}

	ld, err := cl.lastDep(ctx)
	if err != nil {
		return nil, DErrD(err)
	}

	if ld != nil {
		ldi, cerr := ld.Info(ctx, q, details)
		if cerr != nil {
			return nil, cerr
		}

		ci.LD = ldi.(*api.ClusterDeployInfo)
	}

	if details {
		for nn, nd := range cl.Nodes {
			ni, _ := nd.info(ctx, cl, nn, details)
			ci.Nodes = append(ci.Nodes, ni.(*api.NodeInfo))
		}
	}

	return ci, nil
}

func (cl *ClusterDesc)Keygen(ctx context.Context) *xrest.ReqErr {
	dir, err := sshKeygen(ctx)
	if err != nil {
		return DErrM(xrest.GenErr, "Cannot make keys")
	}

	err = dbUpdatePart(ctx, cl, bson.M{"keys": dir})
	if err != nil {
		return DErrD(err)
	}

	if cl.Keys != "" {
		sshKeydrop(ctx, cl.Keys)
	}
	cl.Keys = dir

	return nil
}

type Clusters struct {}

func (_ Clusters)Create(ctx context.Context, p interface{}) (xrest.Obj, *xrest.ReqErr) {
	params := p.(*api.ClusterAdd)
	return &ClusterDesc {
		User:	gctx(ctx).User,
		Name:	params.Name,
		Nodes:	make(map[string]*NodeDesc),
	}, nil
}

func (_ Clusters)Get(ctx context.Context, r *http.Request) (xrest.Obj, *xrest.ReqErr) {
	var cl ClusterDesc

	cerr := objFindForReq(ctx, r, "cid", &cl)
	if cerr != nil {
		return nil, cerr
	}

	return &cl, nil
}

func (_ Clusters)Iterate(ctx context.Context, q url.Values, cb func(context.Context, xrest.Obj) *xrest.ReqErr) *xrest.ReqErr {
	cname := q.Get("name")

	var cl ClusterDesc

	if cname != "" {
		err := dbFind(ctx, nameReq(ctx, cname), &cl).One(&cl)
		if err != nil {
			return DErrD(err)
		}

		return cb(ctx, &cl)
	}

	iter := dbFind(ctx, listReq(ctx, q["label"]), &cl).Iter()
	defer iter.Close()

	for iter.Next(&cl) {
		cerr := cb(ctx, &cl)
		if cerr != nil {
			return cerr
		}
	}

	err := iter.Err()
	if err != nil {
		return DErrD(err)
	}

	return nil
}

type ClKeyProp struct { }

func (_ ClKeyProp)Info(ctx context.Context, o xrest.Obj, q url.Values) (interface{}, *xrest.ReqErr) {
	cl := o.(*ClusterDesc)
	if cl.Keys == "" {
		return nil, DErrM(api.NotFound, "No keys generated/stored")
	}

	pub, err := sshKeyget(ctx, cl.Keys)
	if err != nil {
		log.Printf("sshKeyget error: %s\n", err.Error())
		return nil, DErrM(xrest.GenErr, "Error reading key")
	}

	return &api.ClusterKeyInfo { Key: pub }, nil
}

func (_ ClKeyProp)Upd(ctx context.Context, o xrest.Obj, u interface{}) *xrest.ReqErr {
	return DErrM(xrest.GenErr, "Not updatable")
}

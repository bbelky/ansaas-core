package main

import (
	"context"
	"errors"
	"os"
	"net/http"
	"api"
	"log"
	"fmt"
	"time"
	"xhttp"
	"xrest"
	"encoding/hex"
	"golang.org/x/crypto/bcrypt"
	"github.com/dgrijalva/jwt-go"
)

type DClaims struct {
	*jwt.StandardClaims
	User string	`json:"user"`
}

const defKeyId	= "0"
var defKey []byte
var keyValidDays = 1

func authInit() error {
	var err error

	key := conf.JwtKey
	if Mode != "devel" {
		key = os.Getenv(key)
	}

	defKey, err = hex.DecodeString(key)
	if err != nil {
		return err
	}

	if len(defKey) == 0 {
		return errors.New("Error in key configuration")
	}

	if Mode == "devel" {
		keyValidDays = 16
	}

	return nil
}

func getKey() ([]byte, string) {
	return defKey, defKeyId
}

func lookupKey(kid string) ([]byte, error) {
	if kid == defKeyId {
		return defKey, nil
	}

	return nil, fmt.Errorf("Invalid key ID value %s", kid)
}

func login(p *api.UserLogin) (string, error) {
	if p.Name == "" {
		return "", fmt.Errorf("Empty username")
	}

	if p.Pass ==  "" {
		return "", fmt.Errorf("No password")
	}

	ue, err := dbFindUser(p.Name)
	if err != nil {
		return "", fmt.Errorf("No such user")
	}

	if ue.PassHash == nil || len(ue.PassHash) == 0 {
		/* likely an oauth user */
		return "", fmt.Errorf("No such user")
	}

	err = bcrypt.CompareHashAndPassword(ue.PassHash, []byte(p.Pass))
	if err != nil {
		return "", err
	}

	return p.Name, nil
}

func getAuthToken(w http.ResponseWriter, p *api.UserLogin) string {
	user, err := login(p)
	if err != nil {
		log.Printf("Login failed: %s\n", err.Error())
		http.Error(w, "Bad username or password", http.StatusUnauthorized)
		return ""
	}

	tok := jwt.NewWithClaims(jwt.SigningMethodHS256, &DClaims {
		&jwt.StandardClaims {
			ExpiresAt: time.Now().AddDate(0, 0, keyValidDays).Unix(),
		},
		user,
	})

	key, kid := getKey()
	tok.Header["kid"] = kid
	tk, err := tok.SignedString(key)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return ""
	}

	log.Printf("Logger in %s for %d days\n", user, keyValidDays)
	return tk
}

func checkAuthToken(tk string) string {
	token, err := jwt.ParseWithClaims(tk, &DClaims{}, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		if kid, ok := token.Header["kid"].(string); ok {
			return lookupKey(kid)
		}

		return nil, fmt.Errorf("Unexpected kid value: %v", token.Header["kid"])
	})

	if err != nil {
		log.Printf("Error parsing token: %s\n", err.Error())
		return ""
	}

	return token.Claims.(*DClaims).User
}

func handleUserLogin(w http.ResponseWriter, r *http.Request) {
	var params api.UserLogin

	if xhttp.HandleCORS(w, r) {
		return
	}

	err := xhttp.RReq(r, &params)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	token := getAuthToken(w, &params)
	if token == "" {
		return
	}

	w.Header().Set("X-Subject-Token", token)
	w.WriteHeader(http.StatusOK)
}

type authHandlerCB func(ctx context.Context, w http.ResponseWriter, r *http.Request) *xrest.ReqErr

func authorize(w http.ResponseWriter, r *http.Request) (context.Context, func(context.Context)) {
	token := r.Header.Get("X-Auth-Token")
	if token == "" {
		http.Error(w, "Auth token not provided", http.StatusUnauthorized)
		return nil, nil
	}

	user := checkAuthToken(token)
	if user == "" {
		http.Error(w, "Bad token provided", http.StatusForbidden)
		return nil, nil
	}

	log.Printf("Request [%s%s] from [%s]\n", r.Method, r.URL.Path, user)

	return &dContext{
		context.Background(),
		user,
		session.Copy(),
	}, func(ctx context.Context) { gctx(ctx).S.Close() }
}

func authHandler(cb authHandlerCB) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if xhttp.HandleCORS(w, r) {
			return
		}

		ctx, done := authorize(w, r)
		if ctx == nil {
			return
		}

		defer done(ctx)

		cerr := cb(ctx, w, r)
		if cerr != nil {
			http.Error(w, cerr.String(), ErrCode(cerr))
		}
	})
}


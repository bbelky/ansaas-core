package main

import (
	"context"
	"flag"
	"io/ioutil"
	"log"
	"net/http"
	"time"
	"xhttp"

	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2"
	"gopkg.in/yaml.v2"
)

func readConfig(path string, c interface{}) error {
	yamlFile, err := ioutil.ReadFile(path)
	if err == nil {
		err = yaml.Unmarshal(yamlFile, c)
	}
	return err
}

type dContext struct {
	context.Context
	User string
	S    *mgo.Session
}

func gctx(ctx context.Context) *dContext {
	return ctx.(*dContext)
}

type ConfDaemon struct {
	Addr  string               `yaml:"address"`
	HTTPS *xhttp.YAMLConfHTTPS `yaml:"https,omitempty"`
}

type Config struct {
	Daemon ConfDaemon `yaml:"daemon"`
	DB     string     `yaml:"db"`
	MQ     string     `yaml:"mq"`
	Volume string     `yaml:"volume"`
	JwtKey string     `yaml:"jwtk"`
}

var conf Config
var Mode string

func main() {
	var config_path string

	flag.StringVar(&config_path, "conf", "/etc/ansaas/daemon-conf.yaml", "path to a config file")
	flag.StringVar(&Mode, "mode", "", "mode of operations")
	flag.Parse()

	err := readConfig(config_path, &conf)
	if err != nil {
		log.Printf("Bad config: %s\n", err.Error())
		return
	}

	err = authInit()
	if err != nil {
		log.Printf("Cannot init auth: %s\n", err.Error())
		return
	}

	err = dbConnect()
	if err != nil {
		log.Printf("Cannot connect to DB: %s\n", err.Error())
		return
	}

	err = mqConnect()
	if err != nil {
		log.Printf("Cannot connect to amqp: %s\n", err.Error())
		return
	}

	r := mux.NewRouter()

	r.HandleFunc("/v1/login", handleUserLogin).Methods("POST", "OPTIONS")
	r.Handle("/v1/clusters", authHandler(handleClusters)).Methods("GET", "POST", "OPTIONS")
	r.Handle("/v1/clusters/{cid}", authHandler(handleCluster)).Methods("GET", "DELETE", "OPTIONS")
	r.Handle("/v1/clusters/{cid}/nodes", authHandler(handleClusterNodes)).Methods("GET", "POST", "OPTIONS")
	r.Handle("/v1/clusters/{cid}/nodes/{nname}", authHandler(handleClusterNode)).Methods("GET", "DELETE", "OPTIONS")
	r.Handle("/v1/clusters/{cid}/sshkey", authHandler(handleClusterSSHKey)).Methods("GET", "OPTIONS")
	r.Handle("/v1/clusters/{cid}/sshkey/make", authHandler(handleClusterSSHKeyGen)).Methods("POST", "OPTIONS")
	r.Handle("/v1/clusters/{cid}/deploys", authHandler(handleDeploys)).Methods("GET", "POST", "OPTIONS")
	r.Handle("/v1/clusters/{cid}/deploys/{did}", authHandler(handleDeploy)).Methods("GET", "DELETE", "OPTIONS")
	r.Handle("/v1/clusters/{cid}/deploys/{did}/logs", authHandler(handleDeployLogs)).Methods("GET", "OPTIONS")
	r.Handle("/v1/clusters/{cid}/deploys/{did}/logs/pull", authHandler(handleDeployLogsPull)).Methods("GET", "OPTIONS")
	r.Handle("/v1/clusters/{cid}/deploys/{did}/artifacts", authHandler(handleArtifacts)).Methods("GET", "OPTIONS")
	r.Handle("/v1/clusters/{cid}/deploys/{did}/artifacts/{aname}", authHandler(handleArtifact)).Methods("GET", "OPTIONS")

	log.Printf("Starting http server\n")
	err = xhttp.ListenAndServe(
		&http.Server{
			Handler:      r,
			Addr:         conf.Daemon.Addr,
			WriteTimeout: 60 * time.Second,
			ReadTimeout:  60 * time.Second,
		}, conf.Daemon.HTTPS, true, func(s string) { log.Printf(s) })
	if err != nil {
		log.Printf("ListenAndServe error: %s", err.Error())
	}
}

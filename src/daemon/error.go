package main

import (
	"net/http"
	"xrest"
	"gopkg.in/mgo.v2"
	"api"
)

var gateErrMsg = map[uint]string {
	api.DbError:		"Database request failed",
	api.Duplicate:		"ID already exists",
	api.NotFound:		"ID not found",
	api.NotAvailable:	"Operation not possible",
}

func DErrC(code uint) *xrest.ReqErr {
	return &xrest.ReqErr{code, gateErrMsg[code]}
}

func DErrE(code uint, err error) *xrest.ReqErr {
	return &xrest.ReqErr{code, err.Error()}
}

func DErrM(code uint, msg string) *xrest.ReqErr {
	return &xrest.ReqErr{code, msg}
}

func DErrD(err error) *xrest.ReqErr {
	if err == mgo.ErrNotFound {
		return DErrC(api.NotFound)
	} else if mgo.IsDup(err) {
		return DErrC(api.Duplicate)
	} else {
		return DErrE(api.DbError, err)
	}
}

func ErrCode(ce *xrest.ReqErr) int {
	switch ce.Code {
	case api.NotFound:
		return http.StatusNotFound
	case api.Duplicate:
		return http.StatusConflict
	case xrest.BadRequest:
		return http.StatusBadRequest
	case api.NotAvailable:
		return http.StatusExpectationFailed
	default:
		return http.StatusInternalServerError
	}
}

package main

import (
	"common"
	"log"
	"context"
	"api"
	"xrest"
	"encoding/json"
	"github.com/streadway/amqp"
)

var con *xh.XCreds

func mqPublishAnsibleWork(ctx context.Context, msg *api.DeployMsg) *xrest.ReqErr {
	nConn, err := amqp.Dial("amqp://" + con.URL())
	if err != nil {
		log.Printf("Can't dial amqp: %s", err.Error())
		return DErrM(xrest.GenErr, "Cannot dial amqp")
	}

	defer nConn.Close()

	channel, err := nConn.Channel()
	if err != nil {
		log.Printf("error get chan: %s\n", err.Error())
		return DErrM(xrest.GenErr, "Cannot open channel")
	}

	defer channel.Close()

	data, _ := json.Marshal(msg)
	err = channel.Publish("", api.WorkerQueue, false, false, amqp.Publishing{
		DeliveryMode: amqp.Persistent,
		ContentType: "application/json",
		Body: data,
	})
	if err != nil {
		log.Printf("Failed to send work notification: %s", err.Error())
		return DErrM(xrest.GenErr, "Cannot start worker")
	}

	return nil
}

func mqPublishAbort(ctx context.Context, msg *api.AbortMsg) *xrest.ReqErr {
	nConn, err := amqp.Dial("amqp://" + con.URL())
	if err != nil {
		log.Printf("Can't dial amqp: %s", err.Error())
		return DErrM(xrest.GenErr, "Cannot dial amqp")
	}

	defer nConn.Close()

	channel, err := nConn.Channel()
	if err != nil {
		log.Printf("error get chan: %s\n", err.Error())
		return DErrM(xrest.GenErr, "Cannot open channel")
	}

	defer channel.Close()

	data, _ := json.Marshal(msg)
	err = channel.Publish(api.AbortExchange, "", false, false, amqp.Publishing{
		ContentType: "application/json",
		Body: data,
	})
	if err != nil {
		log.Printf("Failed to send abort notification: %s", err.Error())
		return DErrM(xrest.GenErr, "Cannot stop worker")
	}

	return nil
}

func mqConnect() error {
	con = xh.ParseXCreds(conf.MQ)
	con.Resolve()
	if Mode != "devel" {
		con.UPEnv()
	}

	log.Printf("Connect to mq %s@%s\n", con.User, con.Addr())
	nConn, err := amqp.Dial("amqp://" + con.URL())
	if err != nil {
		log.Printf("Can't dial amqp: %s", err.Error())
		return err
	}

	defer nConn.Close()

	nChan, err := nConn.Channel()
	if err != nil {
		nConn.Close()
		log.Printf("Can't get channel: %s", err.Error())
		return err
	}

	defer nChan.Close()

	_, err = nChan.QueueDeclare(api.WorkerQueue, true, false, false, false, nil)
	if err != nil {
		log.Printf("error queue declare: %s\n", err.Error())
		return err
	}

	err = nChan.ExchangeDeclare(api.AbortExchange, "fanout", false, true, false, false, nil)
	if err != nil {
		log.Printf("error get exchange: %s\n", err.Error())
		return err
	}

	return nil
}

func receiveLogs(jid string, to chan *api.DepLogEntry, stop chan struct{}) error {
	nConn, err := amqp.Dial("amqp://" + con.URL())
	if err != nil {
		log.Printf("Can't dial amqp: %s", err.Error())
		return err
	}

	defer nConn.Close()

	channel, err := nConn.Channel()
	if err != nil {
		log.Printf("error get chan: %s\n", err.Error())
		return err
	}

	q, err := channel.QueueDeclare("", false, true, true, false, nil)
	if err != nil {
		log.Printf("error queue declare: %s\n", err.Error())
		return err
	}

	err = channel.QueueBind(q.Name, jid, api.LogsExchange, false, nil)
	if err != nil {
		log.Printf("error queue bind: %s\n", err.Error())
		return err
	}

	msgs, err := channel.Consume(q.Name, "", true, false, false, false, nil)
	if err != nil {
		log.Printf("error consume: %s\n", err.Error())
		return err
	}

	go func() {
		defer channel.Close()
		defer close(to)

		for {
			select {
			case d := <-msgs:
				dle := &api.DepLogEntry{}
				json.Unmarshal(d.Body, dle)
				to <-dle
				if dle.Type == api.LogFinType {
					log.Printf("Stop getting messages for %s (%s)\n", jid, q.Name)
					return
				}
			case <-stop:
				log.Printf("Abort getting messages for %s (%s)\n", jid, q.Name)
				return
			}
		}
	}()

	return nil
}

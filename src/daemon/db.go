package main

import (
	"log"
	"time"
	"api"
	"errors"
	"context"
	"reflect"
	"xrest"
	"net/http"
	"common"
	"github.com/gorilla/mux"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

var dbname = api.DBStateDB
var session *mgo.Session
var dbNotAllowed = errors.New("Not allowed")

func dbMayModify(ctx context.Context) bool {
	return true
}

func dbMayInsert(ctx context.Context) bool {
	return dbMayModify(ctx)
}

func dbMayRemove(ctx context.Context) bool {
	return dbMayModify(ctx)
}

func dbMayUpdate(ctx context.Context) bool {
	return dbMayModify(ctx)
}

func dbCol(ctx context.Context, col string) *mgo.Collection {
	return gctx(ctx).S.DB(dbname).C(col)
}

func objcni(o interface{}) (string, bson.ObjectId) {
	switch o := o.(type) {
	case *ClusterDesc:
		return api.DBColClusters, o.ObjID
	case *DeployDesc:
		return api.DBColDepJobs, o.ObjID
	default:
		log.Fatalf("Unmapped object %s", reflect.TypeOf(o).String())
		return "", ""
	}
}

func objq(ctx context.Context, o interface{}) (*mgo.Collection, bson.M) {
	col, id := objcni(o)
	return dbCol(ctx, col), bson.M{"_id": id}
}

func dbRemove(ctx context.Context, o interface{}) error {
	if !dbMayRemove(ctx) {
		return dbNotAllowed
	}

	col, q := objq(ctx, o)
	return col.Remove(q)
}

func dbInsert(ctx context.Context, o interface{}) error {
	if !dbMayInsert(ctx) {
		return dbNotAllowed
	}

	col, _ := objq(ctx, o)
	return col.Insert(o)
}

func dbFind(ctx context.Context, q interface{}, o interface{}) *mgo.Query {
	col, _ := objq(ctx, o)
	return col.Find(q)
}

func dbUpdatePart2(ctx context.Context, o interface{}, q2 bson.M, u bson.M) error {
	if !dbMayUpdate(ctx) {
		return dbNotAllowed
	}

	col, q := objq(ctx, o)
	for k, v := range q2 {
		q[k] = v
	}
	return col.Update(q, bson.M{"$set": u})
}

func dbUpdatePart(ctx context.Context, o interface{}, u bson.M) error {
	if !dbMayUpdate(ctx) {
		return dbNotAllowed
	}

	col, q := objq(ctx, o)
	return col.Update(q, bson.M{"$set": u})
}

func dbUpdatePartUn(ctx context.Context, o interface{}, u bson.M) error {
	if !dbMayUpdate(ctx) {
		return dbNotAllowed
	}

	col, q := objq(ctx, o)
	return col.Update(q, bson.M{"$unset": u})
}

func dbUpdateAll(ctx context.Context, o interface{}) error {
	if !dbMayUpdate(ctx) {
		return dbNotAllowed
	}

	col, q := objq(ctx, o)
	return col.Update(q, o)
}

func listReq(ctx context.Context, labels []string) bson.D {
	q := bson.D{{"user", gctx(ctx).User}}
	for _, l := range labels {
		q = append(q, bson.DocElem{"labels", l})
	}
	return q
}

func nameReq(ctx context.Context, name string) bson.M {
	return bson.M{"user": gctx(ctx).User, "name": name}
}

func idReq(ctx context.Context, id string, q bson.M) bson.M {
	if q == nil {
		q = bson.M{}
	}

	q["user"] = gctx(ctx).User
	q["_id"] = bson.ObjectIdHex(id)

	return q
}

func objFindId(ctx context.Context, id string, out interface{}, q bson.M) *xrest.ReqErr {
	if !bson.IsObjectIdHex(id) {
		return DErrM(xrest.BadRequest, "Bad ID value")
	}

	err := dbFind(ctx, idReq(ctx, id, q), out).One(out)
	if err != nil {
		return DErrD(err)
	}

	return nil
}

func objFindForReq2(ctx context.Context, r *http.Request, n string, out interface{}, q bson.M) *xrest.ReqErr {
	return objFindId(ctx, mux.Vars(r)[n], out, q)
}

func objFindForReq(ctx context.Context, r *http.Request, n string, out interface{}) *xrest.ReqErr {
	return objFindForReq2(ctx, r, n, out, nil)
}

func dbFindUser(name string) (*api.UserEntry, error) {
	var ue api.UserEntry

	err := session.Copy().DB(dbname).C(api.DBColUsers).Find(bson.M{"name": name}).One(&ue)
	if err != nil {
		return nil, err
	}

	return &ue, nil
}

func dbConnect() error {
	var err error

	dbc := xh.ParseXCreds(conf.DB)
	dbc.Resolve()
	if Mode != "devel" {
		dbc.UPEnv()
	}
	if dbc.Domn != "" {
		dbname = dbc.Domn
	}

	log.Printf("Connect to db %s@%s\n", dbc.User, dbc.Addr())
	info := mgo.DialInfo{
		Addrs:		[]string{dbc.Addr()},
		Database:	dbname,
		Timeout:	60 * time.Second,
		Username:	dbc.User,
		Password:	dbc.Pass,
	}

	session, err = mgo.DialWithInfo(&info);
	if err != nil {
		log.Printf("dbConnect: Can't dial to %s with db %s (%s)",
				conf.DB, dbname, err.Error())
		return err
	}

	session.SetMode(mgo.Monotonic, true)

	return nil

}

func dbDisconnect() {
	session.Close()
	session = nil
}

package main

import (
	"os"
	"path"
	"api"
	"errors"
	"os/exec"
	"io/ioutil"
	"context"
	"log"
)

func keyDir(ctx context.Context) string {
	return api.KeysDir(conf.Volume, gctx(ctx).User)
}

func pubKey(ctx context.Context, dir string) string {
	return api.PublicKey(conf.Volume, gctx(ctx).User, dir)
}

func sshKeygen(ctx context.Context) (string, error) {
	os.MkdirAll(keyDir(ctx), 0700)
	dir, err  := ioutil.TempDir(keyDir(ctx), "keys")
	if err != nil {
		log.Printf("Cannot make dir: %s\n", err.Error())
		return "", err
	}

	cmd := exec.Command("ssh-keygen", "-q", "-f", dir + "/" + api.Kname, "-P", "")
	err = cmd.Run()
	if err != nil {
		os.RemoveAll(dir)
		return "", errors.New("kgrun:" + err.Error())
	}

	return path.Base(dir), nil
}

func sshKeydrop(ctx context.Context, dir string) {
	os.RemoveAll(keyDir(ctx) + "/" + dir)
}

func sshKeyget(ctx context.Context, dir string) (string, error) {
	data, err := ioutil.ReadFile(pubKey(ctx, dir))
	if err != nil {
		return "", err
	}

	return string(data), nil
}

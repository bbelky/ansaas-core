package main

import (
	"os"
	"log"
	"api"
	"time"
	"sort"
	"xrest"
	"strconv"
	"net/url"
	"context"
	"net/http"
	"io/ioutil"
	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Deploys struct { cl *ClusterDesc }

type DeployDesc struct {
	ObjID		bson.ObjectId		`bson:"_id,omitempty"`
	Started		time.Time		`bson:"started"`
	Cluster		bson.ObjectId		`bson:"cluster_id"`
	User		string			`bson:"user"`
	IPs		[]string		`bson:"ips"`
	Playbook	string			`bson:"playbook"`
	Status		string			`bson:"status"`
	Finished	*time.Time		`bson:"finished,omitempty"`

	Logs		[]*api.DepLogEntry	`bson:"logs"`

	/* Keys path is on-time parameter, not to be kept in DB */
	keys		string			`bson:"-"`
}

func (ds Deploys)Create(ctx context.Context, p interface{}) (xrest.Obj, *xrest.ReqErr) {
	cl := ds.cl

	if cl.Keys == "" {
		return nil, DErrM(api.NotAvailable, "No keys for the cluster")
	}

	if len(cl.Nodes) == 0 {
		return nil, DErrM(api.NotAvailable, "No nodes in cluster")
	}

	dd := &DeployDesc { }
	params := p.(*api.ClusterDeploy)

	dd.ObjID = bson.NewObjectId()
	dd.Cluster = cl.ObjID
	dd.User = cl.User
	dd.keys = cl.Keys
	dd.Playbook = params.Playbook
	for _, nd := range cl.Nodes {
		dd.IPs = append(dd.IPs, nd.IP())
	}
	sort.Strings(dd.IPs)

	return dd, nil
}

func dbFindDep(ctx context.Context, q bson.M) *mgo.Query {
	return dbFind(ctx, q, &DeployDesc{}).Select(bson.M{"logs": 0})
}

func (ds Deploys)Get(ctx context.Context, r *http.Request) (xrest.Obj, *xrest.ReqErr) {
	did := mux.Vars(r)["did"]
	if !bson.IsObjectIdHex(did) {
		return nil, DErrM(xrest.BadRequest, "Bad deploy ID")
	}

	var dd DeployDesc

	err := dbFindDep(ctx, bson.M{"_id": bson.ObjectIdHex(did), "cluster_id": ds.cl.ObjID}).One(&dd)
	if err != nil {
		return nil, DErrD(err)
	}

	return &dd, nil
}

func (ds Deploys)Iterate(ctx context.Context, q url.Values, cb func(context.Context, xrest.Obj) *xrest.ReqErr) *xrest.ReqErr {
	var dd DeployDesc

	iter := dbFindDep(ctx, bson.M{"cluster_id": ds.cl.ObjID}).Iter()
	defer iter.Close()

	for iter.Next(&dd) {
		cerr := cb(ctx, &dd)
		if cerr != nil {
			return cerr
		}
	}

	err := iter.Err()
	if err != nil {
		return DErrD(err)
	}

	return nil
}


func (dd *DeployDesc)amqp() *api.DeployMsg {
	return &api.DeployMsg {
		JobID:		dd.ObjID.Hex(),
		User:		dd.User,
		Playbook:	dd.Playbook,
		IPs:		dd.IPs,

		Keys:		dd.keys,
	}
}

func (dd *DeployDesc)Info(ctx context.Context, q url.Values, details bool) (interface{}, *xrest.ReqErr) {
	di := &api.ClusterDeployInfo {
		Id:		dd.ObjID.Hex(),
		Playbook:	dd.Playbook,
		Started:	dd.Started.Format(time.RFC1123Z),
		Status:		dd.Status,
	}

	if dd.Finished != nil {
		di.Finished = dd.Finished.Format(time.RFC1123Z)
	}

	return di, nil
}

func (dd *DeployDesc)Add(ctx context.Context, _ interface{}) *xrest.ReqErr {
	dd.Started = time.Now()
	dd.Status = api.DeployStarted

	st, err := os.Stat(api.PlaybookDir(conf.Volume, dd.Playbook))
	if err != nil {
		if os.IsNotExist(err) {
			return DErrM(api.NotFound, "No such playbook")
		} else {
			log.Printf("Error in playbook area: %s\n", err.Error())
			return DErrM(xrest.GenErr, "Error accessing playbook")
		}
	} else if !st.IsDir() {
		log.Printf("Error in playbook area for %s\n", dd.Playbook)
		return DErrM(xrest.GenErr, "Error accessing playbook")
	}

	err = dbInsert(ctx, dd)
	if err != nil {
		return DErrD(err)
	}

	return mqPublishAnsibleWork(ctx, dd.amqp())
}

func (dd *DeployDesc)Upd(ctx context.Context, _ interface{}) *xrest.ReqErr {
	return DErrM(xrest.GenErr, "Not updatable")
}

func (dd *DeployDesc)Del(ctx context.Context) *xrest.ReqErr {
	cerr := mqPublishAbort(ctx, &api.AbortMsg{JobID: dd.ObjID.Hex() })
	if cerr != nil {
		return cerr
	}

	adir := api.ArtifactsDir(conf.Volume, gctx(ctx).User, dd.ObjID.Hex())
	err := os.Rename(adir, adir + ".dead")
	if err == nil {
		go func() {
			err = os.RemoveAll(adir + ".dead")
			if err != nil {
				log.Printf("Leak %s: %s\n", adir + ".dead", err.Error())
			}
		}()
	} else if !os.IsNotExist(err) {
		log.Printf("Cannot remove artifacts %s: %s\n", adir, err.Error())
		return DErrM(xrest.GenErr, "Cannot remove artifacts")
	}

	err = dbRemove(ctx, dd)
	if err != nil {
		return DErrD(err)
	}

	return nil
}

type DepLogsProp struct { }

func (_ DepLogsProp)Info(ctx context.Context, o xrest.Obj, q url.Values) (interface{}, *xrest.ReqErr) {
	dep := o.(*DeployDesc)
	var dep_full DeployDesc

	dbq := dbFind(ctx, bson.M{"_id": dep.ObjID}, &dep_full)

	off := q.Get("skip")
	if off != "" {
		off_i, err := strconv.Atoi(off)
		if err != nil {
			return nil, DErrM(xrest.BadRequest, "The skip value should be int'")
		}

		skip_slice := []int{ off_i, 1000 }
		dbq = dbq.Select(bson.M{"logs": bson.M{"$slice": skip_slice}})
	}

	err := dbq.One(&dep_full)
	if err != nil {
		return nil, DErrD(err)
	}

	logs := dep_full.Logs
	sort.SliceStable(logs, func(i,j int) bool { return logs[i].Ts.Before(logs[j].Ts) })

	ret := []*api.LogRecord{}
	for _, le := range logs {
		ret = append(ret, &api.LogRecord {
			Ts: le.Ts.Format(time.RFC1123Z),
			Type: le.Type,
			Text: le.Text,
		})
	}

	return ret, nil
}

func (_ DepLogsProp)Upd(ctx context.Context, o xrest.Obj, p interface{}) *xrest.ReqErr {
	return DErrM(xrest.GenErr, "Not updatable")
}

type DepArtifactsProp struct { }

func (_ DepArtifactsProp)Info(ctx context.Context, o xrest.Obj, q url.Values) (interface{}, *xrest.ReqErr) {
	dep := o.(*DeployDesc)

	adir := api.ArtifactsDir(conf.Volume, gctx(ctx).User, dep.ObjID.Hex())
	d, err := os.Open(adir)
	if err != nil {
		return nil, DErrM(xrest.GenErr, "Cannot find artifacts")
	}

	defer d.Close()
	afiles, err := d.Readdirnames(-1)
	if err != nil {
		return nil, DErrM(xrest.GenErr, "Cannot find artifacts")
	}

	return afiles, nil
}

func (_ DepArtifactsProp)Upd(ctx context.Context, o xrest.Obj, p interface{}) *xrest.ReqErr {
	return DErrM(xrest.GenErr, "Not updatable")
}

func (dep *DeployDesc)Artifact(ctx context.Context, aname string) ([]byte, *xrest.ReqErr) {
	adir := api.ArtifactsDir(conf.Volume, gctx(ctx).User, dep.ObjID.Hex())
	data, err := ioutil.ReadFile(adir + "/" + aname)
	if err != nil {
		if os.IsNotExist(err) {
			return nil, DErrM(api.NotFound, "No such artifact")
		} else {
			return nil, DErrM(xrest.GenErr, "Arror reading artifact")
		}
	}

	return data, nil
}

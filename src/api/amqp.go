package api

const (
	WorkerQueue	= "ansaas_worker"
	AbortExchange	= "ansaas_abort"
	LogsExchange	= "ansaas_wlogs"

	LogFinType	= "fin"
)

type DeployMsg struct {
	JobID		string		`json:"jobid"`
	User		string		`json:"user"`
	IPs		[]string	`json:"ips"`
	Keys		string		`json:"keys"`
	Playbook	string		`json:"playbook"`
}

type AbortMsg struct {
	JobID		string		`json:"jobid"`
}

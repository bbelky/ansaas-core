package api

const (
	Kname = "id"
	keys = "keys"
	artifacts = "artifacts"
	playbooks = "playbooks"
)

func KeysDir(root, user string) string {
	return root + "/" + keys + "/" + user
}

func PlaybookDir(root, pb string) string {
	return root + "/" + playbooks + "/" + pb
}

func PrivateKey(root, user, keys string) string {
	return KeysDir(root, user) + "/" + keys + "/" + Kname
}

func PublicKey(root, user, keys string) string {
	return PrivateKey(root, user, keys) + ".pub"
}

func ArtifactsDir(root, user, jid string) string {
	return root + "/" + artifacts + "/" + user + "/" + jid
}

package main

import (
	"io"
	"os"
	"api"
	"log"
	"time"
	"sync"
	"bufio"
	"os/exec"
	"strings"
	"syscall"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"github.com/streadway/amqp"
)

const outCollectThresh = 8
const outCollectTmo = 4 * time.Second

func keyFile(msg *api.DeployMsg) string {
	return api.PrivateKey(conf.Volume, msg.User, msg.Keys)
}

func artifactsDir(msg *api.DeployMsg) string {
	return api.ArtifactsDir(conf.Volume, msg.User, msg.JobID)
}

func forward(wg *sync.WaitGroup, w string, pipe io.ReadCloser, to chan *api.DepLogEntry) {
	defer wg.Done()

	log.Printf("Std%s started\n", w)

	scanner := bufio.NewScanner(pipe)
	for scanner.Scan() {
		txt := scanner.Text()
		to <-mkle(txt, w)
	}

	log.Printf("Std%s closed\n", w)
}

func mkle(txt, typ string) *api.DepLogEntry {
	return &api.DepLogEntry {
		Ts:	time.Now(),
		Type:	typ,
		Text:	txt,
	}
}

func flush(s *mgo.Session, jid string, text []*api.DepLogEntry) {
	/* FIXME -- saving logs in mongo might not be the best option */
	err := s.DB(dbname).C(api.DBColDepJobs).Update(bson.M{"_id": bson.ObjectIdHex(jid) },
		bson.M{"$push": bson.M { "logs": bson.M{ "$each": text } } })
	if err != nil {
		log.Printf("Error saving logs for %s: %s\n", jid, err.Error())
	}
}

func updateDepStatus(s *mgo.Session, jid string, status string) {
	err := s.DB(dbname).C(api.DBColDepJobs).Update(
		bson.M{
			"_id": bson.ObjectIdHex(jid),
			"status": api.DeployStarted,
		},
		bson.M{"$set": bson.M {
			"status": status,
			"finish": time.Now(),
		},
	})
	if err != nil && err != mgo.ErrNotFound {
		log.Printf("Error updating status for %s: %s\n", jid, err.Error())
	}
}

func save(lch *amqp.Channel, s *mgo.Session, wg *sync.WaitGroup, jid string, sout, serr chan *api.DepLogEntry) {
	defer wg.Done()

	var seq int
	batch := []*api.DepLogEntry{}
	for {
		var le *api.DepLogEntry
		ff := false
		w := ""

		select {
		case <-time.After(outCollectTmo):
			ff = len(batch) != 0
			w = "tmo"
		case le = <-sout:
			w = "out"
		case le = <-serr:
			w = "err"
		}

		if w != "tmo" {
			if le == nil {
				if len(batch) != 0 {
					flush(s, jid, batch)
				}

				log.Printf("Done sending logs\n")
				return
			}

			le.Seq = seq
			seq++
			batch = append(batch, le)
			if lch != nil {
				mqPublishLog(lch, jid, le)
			}
			ff = len(batch) >= outCollectThresh
		}

		if ff {
			flush(s, jid, batch)
			batch = []*api.DepLogEntry{}
		}
	}
}

func runPlaybook(msg *api.DeployMsg) {
	s := session.Copy()
	defer s.Close()

	lch, err := conn.Channel()
	if err != nil {
		log.Printf("Failed to get amqp channel: %s\n", err.Error())
		lch = nil
	} else {
		defer lch.Close()
	}

	err = doRunPlaybook(s, lch, msg)

	/*
	 * First set the status, then send the fin. Message readers
	 * open the queue, then check the status
	 */
	if err == nil {
		updateDepStatus(s, msg.JobID, api.DeploySucceeded)
		log.Printf("Run playbook succeeded\n")
	} else {
		updateDepStatus(s, msg.JobID, api.DeployFailed)
		log.Printf("Run playbook failed: %s\n", err.Error())
	}

	if lch != nil {
		mqPublishLog(lch, msg.JobID, mkle("", api.LogFinType))
	}
}

func doRunPlaybook(s *mgo.Session, lch *amqp.Channel, msg *api.DeployMsg) error {
	pbroot := api.PlaybookDir(conf.Volume, msg.Playbook)
	cmd := exec.Command(pbroot + "/run.sh")
	cmd.Dir = pbroot
	cmd.Env = append(cmd.Env, "KEYS=" + keyFile(msg))
	cmd.Env = append(cmd.Env, "HOSTS=" + strings.Join(msg.IPs, " "))

	adir := artifactsDir(msg)
	err := os.MkdirAll(adir, 0750)
	if err != nil {
		log.Printf("Failed to make [%s]: %s\n", adir, err.Error())
	}
	cmd.Env = append(cmd.Env, "ARTIFACTS=" + adir)

	if Mode == "devel" {
		if cmd.SysProcAttr == nil {
			cmd.SysProcAttr = &syscall.SysProcAttr{}
		}
		cmd.SysProcAttr.Setpgid = true
		abortHandler = func() {
			log.Printf("Killing gprp %d\n", cmd.Process.Pid)
			syscall.Kill(-cmd.Process.Pid, syscall.SIGKILL)
		}
	}

	sout, _ := cmd.StdoutPipe()
	defer sout.Close()
	serr, _ := cmd.StderrPipe()
	defer serr.Close()

	err = cmd.Start()
	if err != nil {
		log.Printf("Start playbook failed: %s\n", err.Error())
		return err
	}

	sout_c := make(chan *api.DepLogEntry)
	serr_c := make(chan *api.DepLogEntry)

	var fwg sync.WaitGroup
	fwg.Add(2)
	var swg sync.WaitGroup
	swg.Add(1)

	go forward(&fwg, "out", sout, sout_c)
	go forward(&fwg, "err", serr, serr_c)
	go save(lch, s, &swg, msg.JobID, sout_c, serr_c)

	fwg.Wait()
	log.Printf("Finished stdios forwarding\n")

	close(sout_c)
	close(serr_c)

	if Mode == "devel" {
		abortHandler = abortNotify
	}

	err = cmd.Wait()
	swg.Wait()

	return err
}

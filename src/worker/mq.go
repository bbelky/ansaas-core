package main

import (
	"log"
	"api"
	"encoding/json"
	"github.com/streadway/amqp"
	"common"
)

var conn *amqp.Connection

func mqStart() {
	c := xh.ParseXCreds(conf.MQ)
	c.Resolve()
	if Mode != "devel" {
		c.UPEnv()
	}

	var err error
	log.Printf("Starting mq listener %s@%s\n", c.User, c.Addr())
	conn, err = amqp.Dial("amqp://" + c.URL())
	if err != nil {
		log.Printf("error dial: %s\n", err.Error())
		return
	}

	err = setupLogs(conn)
	if err != nil {
		log.Printf("error logs setup: %s\n", err.Error())
		return
	}

	stop := make(chan struct{})
	go processWorker(conn, stop)
	go processAborts(conn, stop)

	<-stop /* ANY exit matters */
	log.Printf("Exiting\n")
}

var runningDeployJobId string
var abortHandler func()

func processWorker(conn *amqp.Connection, stop chan struct{}) {
	defer func(){stop<-struct{}{}}()

	channel, err := conn.Channel()
	if err != nil {
		log.Printf("error get chan: %s\n", err.Error())
		return
	}

	q, err := channel.QueueDeclare(api.WorkerQueue, true, false, false, false, nil)
	if err != nil {
		log.Printf("error queue declare: %s\n", err.Error())
		return
	}

	msgs, err := channel.Consume(q.Name, "", false, false, false, false, nil)
	if err != nil {
		log.Printf("error consume: %s\n", err.Error())
		return
	}

	log.Printf("Getting deploy requests\n")
	for d := range msgs {
		log.Printf("mq: Received message [%s]\n", d.Body)

		var dm api.DeployMsg

		err = json.Unmarshal(d.Body, &dm)
		if err != nil {
			log.Printf("`- bad message: %s\n", err.Error())
			continue
		}

		runningDeployJobId = dm.JobID
		log.Printf("Start %s deployment job\n", dm.JobID)

		runPlaybook(&dm)

		runningDeployJobId = ""
		d.Ack(false)
	}

	log.Printf("Worker done\n")
}

func processAborts(conn *amqp.Connection, stop chan struct{}) {
	defer func(){stop<-struct{}{}}()

	channel, err := conn.Channel()
	if err != nil {
		log.Printf("error get chan: %s\n", err.Error())
		return
	}

	err = channel.ExchangeDeclare(api.AbortExchange, "fanout", false, true, false, false, nil)
	if err != nil {
		log.Printf("error get exchange: %s\n", err.Error())
		return
	}

	q, err := channel.QueueDeclare("", false, true, true, false, nil)
	if err != nil {
		log.Printf("error queue declare: %s\n", err.Error())
		return
	}

	err = channel.QueueBind(q.Name, "", api.AbortExchange, false, nil)
	if err != nil {
		log.Printf("error queue bind: %s\n", err.Error())
		return
	}

	msgs, err := channel.Consume(q.Name, "", true, false, false, false, nil)
	if err != nil {
		log.Printf("error consume: %s\n", err.Error())
		return
	}

	log.Printf("Getting aborts\n")
	for d := range msgs {
		log.Printf("mq: Received abort [%s]", d.Body)

		var dm api.AbortMsg

		err = json.Unmarshal(d.Body, &dm)
		if err != nil {
			log.Printf("`- bad message: %s\n", err.Error())
			continue
		}

		if runningDeployJobId == dm.JobID {
			log.Printf("Abort running %s deployment job\n", dm.JobID)
			abortHandler()
		}
	}

	log.Printf("Aborts done\n")
}

func setupLogs(conn *amqp.Connection) error {
	channel, err := conn.Channel()
	if err != nil {
		log.Printf("error get chan: %s\n", err.Error())
		return err
	}

	defer channel.Close()

	err = channel.ExchangeDeclare(api.LogsExchange, "direct", true, false, false, false, nil)
	if err != nil {
		log.Printf("error get log exchange: %s\n", err.Error())
		return err
	}

	return nil
}

func mqPublishLog(ch *amqp.Channel, jid string, le *api.DepLogEntry) {
	data, _ := json.Marshal(le)
	err := ch.Publish(api.LogsExchange, jid, false, false, amqp.Publishing{
		ContentType: "application/json",
		Body: data,
	})
	if err != nil {
		log.Printf("Failed to send log: %s", err.Error())
	}
}

#!/bin/bash

set -x
. ${1}

docker run ${2} -d --name ansaas-worker --net=host		\
	--add-host "mongo.host:${DBADDR}"			\
	-e "DBUSER=${DBUSER}" -e "DBPASS=${DBPASS}"		\
       	--add-host "rabbit.host:${MQADDR}"			\
	-e "MQUSER=${MQUSER}" -e "MQPASS=${MQPASS}"		\
	-v "${VOLUME}:/home/ansaas" ansaas/worker

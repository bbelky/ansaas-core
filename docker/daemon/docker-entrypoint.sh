#!/usr/bin/env bash
VOLUME="/home/ansaas"
mkdir -p ${VOLUME}/keys
mkdir -p ${VOLUME}/artifacts

KSDIR=${VOLUME}/playbooks/kubespray
if ! test -d "${KSDIR}"; then
	mkdir -p ${VOLUME}/playbooks/kubespray
	cd ${KSDIR}
	git clone https://github.com/kubernetes-sigs/kubespray repo
	#release=$(git describe --tags)
	#cd ${KSDIR}/repo && git checkout ${release}
	ln -s /home/pb-runs/kubespray.sh run.sh
	cd ${KSDIR}/repo && git checkout v2.8.3
fi

exec "$@"
#!/bin/bash

REPO="repo"
WDIR=$(mktemp -d /tmp/ksansXXXXXXXX)

function cleanup {
	rm -r "${WDIR}"
}
trap cleanup EXIT

echo "Deploying kubespray"
echo "KEYS:  ${KEYS}"
echo "HOSTS: ${HOSTS}"
echo "ARTIF: ${ARTIFACTS}"
echo "CWD:   $(pwd)"
echo "WDIR:  ${WDIR}"

cp -rfp ${REPO}/inventory/sample ${WDIR}/inv
CONFIG_FILE=${WDIR}/inv/hosts.ini python3 ${REPO}/contrib/inventory_builder/inventory.py ${HOSTS}

set -e

export ANSIBLE_HOST_KEY_CHECKING="False"
ansible-playbook -i "${WDIR}/inv/hosts.ini" --become --become-user=root --key-file "${KEYS}" "${REPO}/cluster.yml"

set -x
scp -o StrictHostKeyChecking=no -i "${KEYS}" "root@${HOSTS// */}:/root/.kube/config" "${ARTIFACTS}/kubeconfig"
echo "OK"
